<?php
$submitClass = realpath( __DIR__ . '/class-amp-cf7-submission.php' );
if ( file_exists( $submitClass ) ) {
	include_once $submitClass;
} else {
	@header( 'HTTP/1.1 500 INTERNAL ERROR' );
	exit;
}

class AMP_Contact_Form_Submit extends AMP_Form_Submit {

	protected $action  = '';
	protected $formId  = 0;
	protected $cf7Vars = array(
		'_wpcf7',
		'_wpcf7_version',
		'_wpcf7_locale',
		'_wpcf7_unit_tag',
	);

	protected $more_Headers = array(
		'Access-Control-Allow-Credentials' => 'true'
	); 

	public function amp_cf7_send_json( $items, $result ) { 

		$status                    = isset( $result['status'] ) ? $result['status'] : '';
		$this->response['message'] = isset( $result['message'] ) ? $result['message'] : '';
		if ( ! empty( $result['invalid_fields'] ) ) {
			$errors = array();
			foreach ( $result['invalid_fields'] as $error ) {
				if ( ! empty( $error['reason'] ) ) {
					$errors[] = array( 'error_detail' => $error['reason'] );
				}
			}
			if ( ! empty( $errors ) ) {
				$this->response['errors']     = $errors;
				$this->response['has_errors'] = true;
			}
		}

		switch ( $status ) {
			case 'mail_failed':
				$this->response['status'] = 'error';
				$this->outputResponse()
				     ->pushResponse( 'HTTP/1.1 500 FORBIDDEN' );
				break;
			case 'mail_sent':
				$this->response['status'] = 'success';

				$this->outputResponse()
				     ->pushResponse();
				break;
			default:
				$this->response['status'] = 'error';
				$this->outputResponse()
				     ->pushResponse( 'HTTP/1.1 403 FORBIDDEN' );
				break;
		}
	}

	protected function preProcess() {

		parent::preProcess();
		if ( ! empty( $this->request['action'] ) ) {
			$this->action = $this->request['action'];
		}
		foreach ( $this->cf7Vars as $var ) {
			unset( $_POST[ $var ] );
			unset( $_REQUEST[ $var ] );
		}

		return $this;
	}

	protected function setHooks() {

		parent::setHooks();

		add_filter( 'wpcf7_ajax_json_echo', array( $this, 'amp_cf7_send_json' ),10, 2 );

		return $this;
	}

	protected function submitForm() {

		$cf7Version = $this->getCf7Version();
		if ( version_compare( $cf7Version, '4.8', 'lt' ) ) {
			foreach ( $this->cf7Vars as $var ) {
				$_POST[ $var ]    = isset( $this->posted[ $var ] ) ? $this->posted[ $var ] : null;
				$_REQUEST[ $var ] = isset( $this->posted[ $var ] ) ? $this->posted[ $var ] : null;
			}
			wpcf7_ajax_json_echo();
		} else {
			$this->wpcf7SubmitForm();
		}
	}

	protected function wpcf7SubmitForm() {

		$id = ampforwp_get_array( $this->posted, '_wpcf7' );
		if ( empty( $id ) ) {
			$this->response['status'] = 'error';
			$this->outputResponse()
			     ->pushResponse( 'HTTP/1.1 404 NOT FOUND' );
		}
		$item = wpcf7_contact_form( $id );

		if ( ! $item ) {
			$this->response['status'] = 'error';
			$this->outputResponse()
			     ->pushResponse( 'HTTP/1.1 404 NOT FOUND' );
		}

		$result = $item->submit();

		$unit_tag = ampforwp_get_array( $this->posted, '_wpcf7_unit_tag' );
		$response = array(
			'into'    => '#' . wpcf7_sanitize_unit_tag( $unit_tag ),
			'status'  => $result['status'],
			'message' => $result['message'],
		);
	
		if ( 'validation_failed' == $result['status'] ) {
			$invalid_fields = array();

			foreach ( (array) $result['invalid_fields'] as $name => $field ) {
				$invalid_fields[] = array(
					'into'    => 'span.wpcf7-form-control-wrap.'
					             . sanitize_html_class( $name ),
					'message' => $field['reason'],
					'idref'   => $field['idref'],
				);
			}

			$response['invalidFields'] = $invalid_fields;
		}

		$this->amp_cf7_send_json( $response, $result );
	}

	protected function run() {

		switch ( $this->action ) {
			case 'submit':
				$this->submitForm();
				break;
			default:
				$this->response['status'] = 'error';
				$this->outputResponse()
				     ->pushResponse( 'HTTP/1.1 404 NOT FOUND' );
				break;
		}

		return $this;
	}

	protected function getCf7Version() {

		$version = ampforwp_get_array( $this->posted, '_wpcf7_version' );
		if ( empty( $version ) && defined( 'WPCF7_VERSION' ) ) {
			$version = WPCF7_VERSION;
		}

		return $version;
	}
}

$amp_cf7_instantiate = new AMP_Contact_Form_Submit( $_GET, $_POST );
$amp_cf7_instantiate->activate();