<?php
/*
 * Plugin Name: Contact Form 7 for AMP
 * Plugin URI: https://ampforwp.com/contact-form-7/
 * Description: Add CF7 integration with AMP
 * Author: Mohammed Kaludi
 * Version: 1.4
 * Author URI: https://www.ampforwp.com/
 * License: GPL2+
 * Text Domain: ampforwp
 *
 */
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;

define('AMP_CF7_PLUGIN_URI', plugin_dir_url(__FILE__));
define('AMP_CF7_PLUGIN_PATH', plugin_dir_path(__FILE__));

add_action('init', 'amp_cf7_plugin_init');
function amp_cf7_plugin_init() { 
    define( AMPFORWP_AMP__DIR__, AMP__DIR__);

    require_once AMP_CF7_PLUGIN_PATH .'/class-amp-cf7-blacklist.php';
}

add_action('wp', 'ampforwp_start_generating_code', 100);
function ampforwp_start_generating_code() {

	//if ( function_exists( 'ampforwp_is_amp_endpoint' ) && ampforwp_is_amp_endpoint() ) {
		global $cf7data;
		global $shortcode_tags;

		// store current handler
		$cf7data = ampforwp_get_array( $shortcode_tags, 'contact-form-7' );

		// replace original handler with our
		add_shortcode(
			'contact-form-7','ampforwp_generate_amp_cf7_form'
		);
		add_shortcode(
			'contact-form', 'ampforwp_generate_amp_cf7_form' );
	//}
}

function ampforwp_generate_amp_cf7_form($atts, $content = null, $code = '') { 
	global $cf7data;
	$post_id 	= '';

	$post_id 	= (int) $atts['id'];
	$handler    = $cf7data;
	$cf7Content = $handler( $atts, $content, $code );
	$submit_url =  AMP_CF7_PLUGIN_URI . "/class-amp-cf7-form.php?action=submit";
	$actionXhrUrl = preg_replace('#^https?:#', '', $submit_url);

	// insert updated URL
	$cf7Content = preg_replace( '#action="([^"]+)"#ius', 'action-xhr="' . $actionXhrUrl . '" target="_blank"', $cf7Content ); 

	// Generate Submit Button
	$submit_button =  ampforwp_after_submit_notice();

	//$cf7Content = preg_replace( '#(<p><input\s+type="submit")#iu', $submit_button . '$1', $cf7Content );
	
	$cf7Content = preg_replace( '#(<div class="contact-form-bottom)#iu', $submit_button . '$1', $cf7Content );
	
	$cf7Content = str_replace( 'aria-required="true"', 'required' , $cf7Content );
	$cf7Content = str_replace( '<textarea', '<textarea required' , $cf7Content );
	
	$cf7Content = str_replace( 'novalidate="novalidate"', '' , $cf7Content );
	
	
	
	$content = $cf7Content ;
	
	

	add_filter('amp_post_template_data','ampforwp_add_required_scripts', 20);
	add_action('amp_post_template_css', 'amp_cf7_form_styling');

	return $content;
}
//Add the mustache script
add_action('amp_post_template_head','amp_cf7_add_mustache');
function amp_cf7_add_mustache(){
	global $redux_builder_amp;
	$post_id = '';
	$cf7_form_checker = '';
	if( is_home() && $redux_builder_amp['amp-frontpage-select-option'] ) {
		$post_id = $redux_builder_amp['amp-frontpage-select-option-pages'] ;
		$cf7_form_checker = get_post_meta($post_id, 'amp-cf7-form-checker', true); 
		if ( ! empty( $cf7_form_checker )) { ?>
			<script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
		<?php }
	}
}
function ampforwp_add_required_scripts($data) { 		

	if ( is_singular() || is_home()) {
		// Adding Form Script
		if ( empty( $data['amp_component_scripts']['amp-form'] ) ) {
			$data['amp_component_scripts']['amp-form'] = 'https://cdn.ampproject.org/v0/amp-form-0.1.js';
		}
		// Adding Mustache Script
		if ( empty( $data['amp_component_template']['amp-mustache'] ) ) {
			$data['amp_component_template']['amp-mustache'] = 'https://cdn.ampproject.org/v0/amp-mustache-0.1.js';
		}
	}

	return $data;
}

add_filter('amp_content_sanitizers','ampforwp_add_custom_blacklist', 100);
function ampforwp_add_custom_blacklist($data){
	global $redux_builder_amp;
	$post_id = '';
	$cf7_form_checker  = '';
	$post_id = get_the_ID();
	if( is_home() && $redux_builder_amp['amp-frontpage-select-option'] ) {
		$post_id = $redux_builder_amp['amp-frontpage-select-option-pages'] ;
	}
	$cf7_form_checker = get_post_meta($post_id, 'amp-cf7-form-checker', true); 
	// This will make sure the sanitizer will only run 
	// When post has CF7 form
	if ( ! empty( $cf7_form_checker )) {

		unset($data['AMP_Blacklist_Sanitizer']);
		unset($data['AMPFORWP_Blacklist_Sanitizer']);
		$data[ 'AMPFORWP_CF7_Blacklist' ] = array();

		update_post_meta($post_id, 'amp-cf7-form-checker', 1);
	}

	return $data;
}
add_filter('the_content','ampforwp_check_cf7_shortcode');
function ampforwp_check_cf7_shortcode($content){
	global $redux_builder_amp;
	$post_id = '';
	$cf7_form_checker  = '';

	$post_id = get_the_ID();
	if( is_home() && $redux_builder_amp['amp-frontpage-select-option'] ) {
		$post_id = $redux_builder_amp['amp-frontpage-select-option-pages'] ;
	}
	$cf7_form_checker = get_post_meta($post_id, 'amp-cf7-form-checker', true); 

	if ( has_shortcode($content, 'contact-form-7' ) && empty($cf7_form_checker) ) {
		update_post_meta($post_id, 'amp-cf7-form-checker', 1);
	}
	return $content;
}

function ampforwp_after_submit_notice(){  
	$output = '';
	$output = '
		<div submit-success class="ampforwp-form-status-success-new">
			<template type="amp-mustache">
				<div class="ampforwp-form-status amp_cf7_success">
					{{message}} 
				</div> 
			</template>
		</div>
		<div submit-error>
			<template type="amp-mustache">
				<div class="ampforwp-form-status amp_cf7_error">
					{{message}}
				</div>
				{{#has_errors}}<ul>{{/has_errors}}{{#errors}}<li>{{{error_detail}}}</li>{{/errors}}{{#has_errors}}</ul>{{/has_errors}}
			</template>
		</div>';
	return $output;
}
	 
if ( ! function_exists( 'ampforwp_get_array' ) ) {
	function ampforwp_get_array( $array, $keys, $default = null ) {

		if ( empty( $keys ) && $keys !== 0 ) {
			return $array;
		}

		if ( ! is_array( $array ) ) {
			return $default;
		}

		if ( ! is_array( $keys ) && isset( $array[ $keys ] ) ) {
			return $array[ $keys ];
		}

		if ( is_array( $keys ) ) {
			$current = $array;
			foreach ( $keys as $key ) {
				if ( ! array_key_exists( $key, $current ) ) {
					return $default;
				}

				$current = $current[ $key ];
			}

			return $current;
		}

		return $default;
	}
} 


add_action( 'amp_post_template_head', 'ampforwp_add_post_template_scripts' );
function ampforwp_add_post_template_scripts( $amp_template ) {
	$scripts = $amp_template->get( 'amp_component_template', array() );
	if ( $scripts) { 
		foreach ( $scripts as $template => $script ) : ?>
			<script custom-template="<?php echo esc_attr( $template ); ?>" src="<?php echo esc_url( $script ); ?>" async></script>
		<?php endforeach;
	}
}
function amp_cf7_form_styling(){ ?>.wpcf7 label{font-size:11px;color:#555;letter-spacing:.5px;text-transform:uppercase;line-height:1}.wpcf7-form-control{padding:10px 9px;border-radius:2px;border:1px solid #ccc;font-size:14px}.wpcf7 form{margin-bottom:40px;font-family:sans-serif}.wpcf7 p{margin-bottom:15px}.wpcf7-text{color:#111;width:240px}.wpcf7 .wpcf7-form-control:focus{outline:none}.wpcf7-submit{background:#333;border:0;font-size:11px;padding:15px 30px;text-transform:uppercase;margin-top:-5px;letter-spacing:2px;font-weight:700;color:#fff;box-shadow:2px 3px 6px rgba(102,102,102,0.33)}.ampforwp-form-status{margin-top:-10px;background:#FFF9C4;padding:10px 17px;margin-bottom:20px;font-size:15px;border:1px solid rgba(0,0,0,0.14)}.amp_cf7_error{background:#FFF9C4}.amp_cf7_success{background:#DCEDC8}<?php }

//***************************//
// Updater code Starts here //
//**************************//
/* Plugin Updater */
add_action( 'init', 'amp_cf7_updater' );
/**
   * Load and Activate Plugin Updater Class.
   * @since 0.1.0
   */
  function amp_cf7_updater() {

      /* Load Plugin Updater */
      require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . '/updater/update.php' );

      /* Updater Config */
      $config = array(
          'base'         => plugin_basename( __FILE__ ), //required
          'repo_uri'     => 'http://magazine3.com/updates/',
          'repo_slug'    => 'ampforwp-cf7',
      );
      /* Load Updater Class */
      new AMPFORWP_cf7_Updater( $config );
}