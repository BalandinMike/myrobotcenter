<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage MyRobotCenter
 * @since MyRobotCenter 1.0
 */
?>
	
	<footer class="footer" itemscope itemtype="https://schema.org/Organization">
    <div class="max-width">
        <ul class="footer-list" itemscope itemtype="https://schema.org/PostalAddress">
            <li itemprop="name">myRobotcenter GmbH</li>
            <li itemprop="streetAddress"><?php _e( 'streetAddress', 'myrobotcenter' ); ?></li>
            <li itemprop="postalCode"><?php _e( 'PostalCode', 'myrobotcenter' ); ?></li>
            <li itemprop="addressLocality"><?php _e( 'AddressLocality', 'myrobotcenter' ); ?></li>
            <li itemprop="email"><?php _e( 'email', 'myrobotcenter' ); ?></li>
            <li>www.myrobotcenter.com</li>
            <li itemprop="telephone">00800 500 500 10</li>
        </ul>

        <ul class="footer-nav-list">
            <li><a href="https://<?php _e( 'gtc_url', 'myrobotcenter' ); ?>/" target="_blank"><?php _e( 'GTC', 'myrobotcenter' ); ?></a></li>
            <li><a href="https://<?php _e( 'legal_information_url', 'myrobotcenter' ); ?>/" target="_blank"><?php _e( 'Legal information', 'myrobotcenter' ); ?></a></li>
            <li><a href="https://<?php _e( 'privacy_policy_url', 'myrobotcenter' ); ?>/" target="_blank"><?php _e( 'Privacy Policy', 'myrobotcenter' ); ?></a></li>
            <li><a href="https://<?php _e( 'right_to_cancel_url', 'myrobotcenter' ); ?>/" target="_blank"><?php _e( 'Right to cancel', 'myrobotcenter' ); ?></a></li>
        </ul>
    </div>
	
	<?php wp_footer(); ?>
</footer>



		</div>
		
		<?php get_template_part( 'template-parts/cookie' ); ?>
		
		<script src="<?php //echo get_template_directory_uri() . '/js/script.min.js'; ?>"></script>
		<script type="text/javascript">
            web.startComponents([
			{"name":"lazyload","instanceId":"lazyload-1","options":{}},
			{"name":"header-position","instanceId":"header-position-1","options":{}},
			{"name":"anchor-link","instanceId":"menu-item-66","options":{}},
			{"name":"anchor-link","instanceId":"menu-item-67","options":{}},
			{"name":"anchor-link","instanceId":"menu-item-68","options":{}},
			{"name":"location-chooser","instanceId":"location-chooser-1","options":{}},
			{"name":"mobile-menu","instanceId":"mobile-menu-1","options":{}},
			{"name":"header-slider","instanceId":"header-slider-1","options":{}},
			{"name":"anchor-link","instanceId":"anchor-link-4","options":{}},
			{"name":"anchor-link","instanceId":"anchor-link-5","options":{}},
			{"name":"anchor-link","instanceId":"anchor-link-6","options":{}},
			{"name":"tracking","instanceId":"tracking-1","options":{}},
			{"name":"product-gallery","instanceId":"product-gallery-1","options":{}},
			{"name":"tracking","instanceId":"tracking-2","options":{}},
			{"name":"tracking","instanceId":"tracking-3","options":{}},
			{"name":"product-gallery","instanceId":"product-gallery-2","options":{}},
			{"name":"tracking","instanceId":"tracking-4","options":{}},
			{"name":"reviews-slider","instanceId":"reviews-slider-2","options":{}},
			{"name":"tracking","instanceId":"tracking-5","options":{}},
			{"name":"product-gallery","instanceId":"product-gallery-3","options":{}},
			{"name":"tracking","instanceId":"tracking-6","options":{}},
			{"name":"reviews-slider","instanceId":"reviews-slider-3","options":{}},
			//{"name":"contact-form","instanceId":"contact-form-1","options":{}},
			{"name":"cookie","instanceId":"cookie-1","options":{}}
			]);
        </script>


</body>
</html>
