<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage MyRobotCenter
 * @since MyRobotCenter 1.0
 */

 define('AMP_MODE', true);
 
 if (AMP_MODE) {
	 
	get_template_part( 'amp/index' ); 
	return;
 }
 
get_header(); ?>

<?php get_template_part( 'template-parts/sliders/header-slider' ); ?>


<?php 
$post_no = 1;
$tracking_index = 1;

query_posts(array('orderby'=>'date','order'=>'ASC'));

if ( have_posts() ) : 

	while ( have_posts() ) : the_post();

		$enabled = get_post_meta(get_the_ID(), 'enabled', true);
									
		if ($enabled) {
			set_query_var( 'post_no', $post_no++);
			get_template_part( 'template-parts/product-items' ); 
		}
	endwhile;
		
endif;
?>

<?php get_template_part( 'template-parts/info' ); ?>
<?php get_template_part( 'template-parts/service-buttons' ); ?>
<?php get_template_part( 'template-parts/contact-form' ); ?>
	
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
