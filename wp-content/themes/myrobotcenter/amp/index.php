<?php

global $q_config;	

global $lang;

$lang = empty($q_config['language']) ? 
	substr(get_bloginfo("language"),0, 2): $q_config['language'];


?>

<!doctype html>
<html amp lang="<?php echo $lang; ?>">
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <title><?php bloginfo( 'name' ); ?></title>
	<meta name="description" content="<?php bloginfo( 'description' ); ?>"/>
	<meta name="keywords" content="<?php _e( 'meta_keywords', 'myrobotcenter' ); ?>"/>
	
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <meta name="mobile-web-app-capable" content="yes"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	
	
	<!-- CUSTOM META TAGS -->
	<meta name="language" content="<?php echo $lang; ?>"/>
	<meta name="publisher" content="<?php _e( 'MASSIVE ART WebServices GmbH', 'myrobotcenter' ); ?>"/>
	<meta name="author" content="<?php _e( 'myRobotcenter GmbH', 'myrobotcenter' ); ?>"/>
	<meta name="copyright" content="<?php _e( 'myRobotcenter GmbH', 'myrobotcenter' ); ?>"/>
	<meta name="audience" content="all"/>
	<meta name="distribution" content="global"/>
	<meta name="image" content="<?php  echo get_template_directory_uri() ?>/images/metaimage.jpg"/>

	
	<!-- OG TAGS -->
	<meta property="og:site_name" content="<?php _e( 'myRobotcenter GmbH', 'myrobotcenter' ); ?>"/>
	<meta property="og:url" content="/"/>
	<meta property="og:title" content="<?php bloginfo( 'name' ); ?>"/>
	<meta property="og:type" content="website"/>
	<meta property="og:image" content="<?php  echo get_template_directory_uri() ?>/images/metaimage.jpg"/>
	<meta property="og:description" content="<?php bloginfo( 'description' ); ?>"/>
	<meta property="og:locale" content="<?php echo $lang; ?>"/>
		
	<!-- TWITTER TAGS -->
	<meta name="twitter:card" content="summary"/>
	<meta name="twitter:url" content="/"/>
	<meta name="twitter:title" content="<?php bloginfo( 'name' ); ?>"/>
	<meta name="twitter:description" content="<?php bloginfo( 'description' ); ?>"/>
	<meta name="twitter:image" content="<?php  echo get_template_directory_uri() ?>/images/metaimage.jpg"/>
		

	<!-- DC TAGS -->
	<meta name="DC.Title" content="<?php _e( 'myRobotcenter GmbH', 'myrobotcenter' ); ?>"/>
	<meta name="DC.Publisher" content="<?php _e( 'MASSIVE ART WebServices GmbH', 'myrobotcenter' ); ?>"/>
	<meta name="DC.Copyright" content="<?php _e( 'myRobotcenter GmbH', 'myrobotcenter' ); ?>"/>

	
	
	<link rel="apple-touch-icon" sizes="57x57" href="<?php  echo get_template_directory_uri() ?>/images/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php  echo get_template_directory_uri() ?>/images/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php  echo get_template_directory_uri() ?>/images/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php  echo get_template_directory_uri() ?>/images/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php  echo get_template_directory_uri() ?>/images/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php  echo get_template_directory_uri() ?>/images/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php  echo get_template_directory_uri() ?>/images/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php  echo get_template_directory_uri() ?>/images/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php  echo get_template_directory_uri() ?>/images/favicons/apple-touch-icon-180x180.png">	
			
	<link rel="icon" type="image/png" href="<?php  echo get_template_directory_uri() ?>/images/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php  echo get_template_directory_uri() ?>/images/favicons/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php  echo get_template_directory_uri() ?>/images/favicons/favicon-16x16.png" sizes="16x16">
	<!--
	<link rel="manifest" href="<?php  echo get_template_directory_uri() ?>/images/favicons/manifest.json">
	-->
	<meta name="msapplication-TileColor" content="#00aba9">
	<meta name="msapplication-TileImage" content="<?php  echo get_template_directory_uri() ?>/images/favicons/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	
	
	<link rel="profile" href="http://gmpg.org/xfn/11">
	
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	
	
	<?php get_canonical(); ?>
	
	
	
	<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
	
	<script type="application/ld+json">
      {
        "@context": "http://schema.org",
        "@type": "WebPage",
        "headline": "<?php bloginfo( 'name' ); ?>",
        "datePublished": "2017-10-25T12:02:41Z",
		"image": [
          "<?php  echo get_template_directory_uri() ?>/images/logo.png"
        ],
		"author": {
			"@type": "Organization",
			"name": "myRobotcenter GmbH"
        },
        "dateModified": "2017-10-28T10:19:00Z"

      }
    </script>
	
	<style amp-custom>
		<?php 
		
			$css = file_get_contents(
				get_template_directory_uri() 
					."/css/style-base-min.css"
			);
			
			echo str_replace('[template-path]',
				get_template_directory_uri(), $css);
			
			
		?>
	</style>
	<!--
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
  -->
  
	<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
	<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
	<script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
	<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
	<script async custom-element="amp-selector" src="https://cdn.ampproject.org/v0/amp-selector-0.1.js"></script>
	<script async custom-element="amp-user-notification" src="https://cdn.ampproject.org/v0/amp-user-notification-0.1.js"></script>
	<script async custom-element="amp-image-lightbox" src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>
	
	<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
	<script async custom-element="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
	
	
	
	
  </head>
  <body>
    <amp-sidebar id="sidebar-lang"
		layout="nodisplay"
		class="location-chooser-amp"
		side="left">
		<span class= "close-sidebar" 
			role="button"
			tabindex="0"
			on="tap:sidebar-lang.close">
				&#10005;
		</span>
		<?php get_template_part( 'amp/template-parts/location-chooser' ); ?>

	</amp-sidebar>
	
	<amp-sidebar id="sidebar-anchor"
		layout="nodisplay"
		
		side="left">
		<span class= "close-sidebar" 
			role="button"
			tabindex="1"
			on="tap:sidebar-anchor.close">
			&#10005;
		</span>
		<?php
		wp_nav_menu( array( 
							'theme_location' => 'mrc-top-menu', 
							'container_class' => 'sidebar',
							'container_id' => 'anchor-container',
							'container' => 'nav') ); 
		?>				
	</amp-sidebar>
	
	
	<div class="main-wrapper">
        
			<?php get_template_part( 'amp/template-parts/header_menu' ); ?>

			
			<?php get_template_part( 'amp/template-parts/header-slider' ); ?>


			<?php 
			$post_no = 1;
			global $tracking_index;
			$tracking_index = 1;
			
			global $analytics_data;
			
			query_posts(array('orderby'=>'date','order'=>'ASC'));

			if ( have_posts() ) : 

				while ( have_posts() ) : the_post();

					$enabled = get_post_meta(get_the_ID(), 'enabled', true);
												
					if ($enabled) {
						set_query_var( 'post_no', $post_no++);
						get_template_part( 'amp/template-parts/product-items' ); 
					}
				endwhile;
					
			endif;
			?>

			<?php get_template_part( 'amp/template-parts/info' ); ?>
			<?php get_template_part( 'amp/template-parts/service-buttons' ); ?>
			<?php get_template_part( 'amp/template-parts/contact-form' ); ?>
			<?php get_template_part( 'amp/footer' ); ?>
	
			


		<amp-analytics type="googleanalytics">
		<script type="application/json">
  
		{"vars": {
			"account": "UA-3788284-12"
			},
			"triggers": {
				"TrackPageview": {
					"on": "visible",
					"request": "pageview"
				},
				
				<?php foreach ($analytics_data as $key => $data) {?>
				
				"tracking-<?php echo $data['index'] ?>": {
					"on": "click",
					"selector": "#tracking-<?php echo $data['index'] ?>",
					"request": "event",
					"vars": {
						
						"eventCategory": "<?php echo $data['category'] ?>",
						"eventAction": "<?php echo $data['action'] ?>",
						"eventLabel": "<?php echo $data['label'] ?>"
					}
				} <?php echo ($key == count($analytics_data)-1) ? '' : ','; ?>
				
				<?php } ?>
				
			}
	
		}
		</script>
	</amp-analytics>
	
	</div>
	
	<?php get_template_part( 'amp/template-parts/cookie' ); ?>
		
	<amp-image-lightbox id="lightbox1"
		data-close-button-aria-label="1"
		layout="nodisplay"></amp-image-lightbox>
	
	<div class="toTop">
		<?php get_template_part( 'amp/template-parts/header_menu' ); ?>

	</div>
	
  </body>
</html>