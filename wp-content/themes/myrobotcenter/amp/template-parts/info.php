<?php




?>


<section class="info-block-section">
    <div class="max-width">
        <div class="info-block-small-container">
            
			<amp-img src="<?php echo get_template_directory_uri() ?>/images/mrc-logo.png"
							width="170"
							height="52"
							layout="fixed"
							class="info-block-logo"
							alt="<?php _e( 'Robot', 'myrobotcenter' ); ?>">
			</amp-img>	 
				 
				 
            <h2 class="headline-second">                            
				<?php _e( 'A One-Stop-Service.', 'myrobotcenter' ); ?>
			</h2>

            <div class="info-block-subtitle">
				<?php _e( 'From Guidance, to Purchase over Service and Guarantee.', 'myrobotcenter' ); ?>
			</div>

            <div class="info-block-text">
                            
				<?php _e( 'Since the end', 'myrobotcenter' ); ?>
			
			</div>

            <div class="social-block">
                <div class="grid grid--middle">
                    <div class="grid__item one-half iphoneh--one-whole palm--one-whole">
                        <h3 class="social-title">                             
							<?php _e( 'Stay connected to us!', 'myrobotcenter' ); ?>	
						</h3>

						
                            <a href="<?php _e( 'facebook_url', 'myrobotcenter' ); ?>"
                               target="_blank"
                               title="<?php _e( 'Like us on Facebook', 'myrobotcenter' ); ?>"
                               class="social-item">
                                <div class="social-icon facebook">
                                    <i class="icon-facebook"></i>
                                </div>
								
								
								
								
								
                                <div class="social-text">
                                    
									<?php _e( 'Like us on Facebook', 'myrobotcenter' ); ?>	
							
								</div>
                            </a>
                             <a href="<?php _e( 'youtube_url', 'myrobotcenter' ); ?>"
                               target="_blank"
                               title="<?php _e( 'Subscribe to our YouTube channel', 'myrobotcenter' ); ?>"
                               class="social-item">
                                <div class="social-icon youtube">
                                    <i class="icon-youtube"></i>
                                </div>
                                <div class="social-text">
                                    <?php _e( 'Subscribe to our YouTube channel', 'myrobotcenter' ); ?> 
								</div>
                            </a>
                                            </div>
                    <div class="grid__item one-half iphoneh--one-whole palm--one-whole">
                       					
						
                    
						<amp-img src="<?php echo get_template_directory_uri() ?>/images/robot.png"
							width="460"
							height="306"
							layout="responsive"
							class="info-block-image"
							alt="<?php _e( 'Robot', 'myrobotcenter' ); ?>">
						</amp-img>
					
					
					</div>
                </div>
            </div>
        </div>
    </div>
</section>