<?php

global $lang;

?>

<header id="header-position-1" class="header js-header">
				

				<div class="header-inner">
					<div class="logo-container">
						
						
					<amp-img src="<?php  echo get_template_directory_uri() ?>/images/logo.png"
						width="120"
						height="21"
						layout="responsive"
						alt="myVacBot"
						class="header-logo">
					</amp-img>	
						
						
					<h1><?php _e( 'myVacBot', 'myrobotcenter' ); ?></h1>
				</div>
					
					
				<?php
					wp_nav_menu( array( 
						'theme_location' => 'mrc-top-menu', 
						'container_class' => 'anchor-container',
						'container_id' => 'anchor-container',
						'container' => 'nav') ); 
								
				?>
					
			
					
					
				<div class="location-chooser"  on="tap:sidebar-lang.toggle" role="button"
					tabindex="2">
						<div id="location-chooser-1" class="choose-location clickable">
							<span class="select-text">
								<?php _e( 'Choose country', 'myrobotcenter' ); ?>	
							</span>
							<span class="location-icon">
								<i class="flag-icon <?php echo $lang; ?>"></i>
							</span>
						</div>
				</div>
					
					
					<div class="mobile-menu" 
					on="tap:sidebar-anchor.toggle"
					role="button"
					tabindex="3">
						<button id="mobile-menu-1" class="hamburger">
							<span class="hamburger-stripe"></span>
						</button>
					</div>
				</div>
			</header>