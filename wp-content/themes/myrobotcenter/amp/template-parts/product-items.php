<?php

/** @var int $post_no */

global $tracking_index;
global $analytics_data;

$product_image = get_post_meta(get_the_ID(), 
	'product-image', true);

$options = get_post_meta(get_the_ID(), 'options', true);

$option_arr = array();

if (!empty($options))	
	$option_arr = explode(',', $options);
	
$slider_images = get_post_meta(get_the_ID(), 'slider-images', true);

$slider_img_arr = array();

if (!empty($slider_images))	
	$slider_img_arr = explode(',', $slider_images);


$pdf_link = get_post_meta(get_the_ID(), 'pdf-link', true);


$features_short_description = get_post_meta(get_the_ID(), 'features-short-description', true);

$features_bottom_block = get_post_meta(get_the_ID(), 'features-bottom-block', true);



$features_bottom_block = str_replace('{template-path}',
	get_template_directory_uri() ,$features_bottom_block);

?>




	<article id="product-item-anchor-<?php echo $post_no; ?>"
         class="product-item panel"
         itemscope
         itemtype="https://schema.org/Product">

    <!-- Product header. -->
    <header class="product-item-header">
        <div class="max-width">
            <div class="grid grid--middle">
                <div class="grid__item one-half iphoneh--one-whole palm--one-whole">
                    <div class="product-image-container">
                        
						
							 
						<amp-img src="<?php echo get_template_directory_uri() ?>/images/products/<?php echo $product_image; ?>"
							
							width="420"
							height="434"
							layout="responsive"
							class="product-image"
							alt="<?php the_title(); ?>">
						</amp-img>

						
							 
                    </div>
                </div>
                <div class="grid__item one-half iphoneh--one-whole palm--one-whole">
                    <h2 class="headline-second-special" itemprop="name">
                        <?php the_title(); ?>                    </h2>

                                            <div class="product-subtitle">
												<?php echo strip_tags(get_the_content()) ;?>
											</div>
											<?php
											
											if (count($option_arr) > 0) {
											
												
											?>
                                            <ul class="product-desc-list">
												<?php foreach ($option_arr as $opt) { ?>
											
                                                <li class="product-desc-list-item">
													<?php echo $opt; ?>                                    
												</li>
												
												<?php } ?>
												
                                            </ul>
                    
											<?php } 
											
											
												$price = get_post_meta(get_the_ID(), 'price', true);
												$currency = get_post_meta(get_the_ID(), 'currency', true);
											
												$pound = '';
												if ($currency == '£'){
													$pound = $currency;
													$currency = '';
												}	
												

											
											?>
					
                                            <div class="product-price-box">
                                                <div class="product-current-price"
													 itemprop="offers"
													 itemscope
													 itemtype="https://schema.org/Offer">

													<meta itemprop="priceCurrency"
														  content="<?php echo $currency; ?>"/>
													<meta itemprop="price"
														  content="<?php echo $price; ?>"/>
													<?php echo $pound . ' ' .$price . ' ' . $currency; ?>   
													
												</div>
                            
                                            </div>
						<?php 
						
							$product_link = get_post_meta(get_the_ID(), 'product-link', true);
							
							$analytics_data[] = array(
								'index' => $tracking_index,
								'action' => 'External Link - Click',
								'category' => 'Link',
								'label' => 'External Link Button:' . __( 'Buy now!', 'myrobotcenter' ) . ' - Link: '. $product_link,
								
								); 
						
						?>
						<div class="btn-container clearfix">
                            <a id="tracking-<?php echo $tracking_index++; ?>"
                               href="<?php echo $product_link; ?>"
                               title="<?php _e( 'Buy now!', 'myrobotcenter' ); ?>"
                               target="_blank"
                               class="btn"
                               data-tracking-category="Link"
                               data-tracking-action="External Link - Click"
                               data-tracking-label="External Link Button: <?php _e( 'Buy now!', 'myrobotcenter' ); ?> - Link: <?php echo $product_link; ?>">
                                <?php _e( 'Buy now!', 'myrobotcenter' ); ?>                            </a>
                        
                        </div>
                </div>
            </div>
        </div>
    </header>

	<?php if (count($slider_img_arr) > 0) { ?>
		
    <!-- Product gallery. -->
            
                
			<amp-carousel 
				  id="product-gallery-<?php echo $post_no; ?>"
				  
				  height="226"
				  layout="responsive"
				  class="product-gallery slick-slider"
				  type="carousel"	  
				   [slide]="selectedSlideProd<?php echo $post_no; ?>" 
				  on="slideChange:AMP.setState({selectedSlideProd<?php echo $post_no; ?>: event.index})">
					
				
				<?php foreach ($slider_img_arr as $img_key => $img) { 
				
					if (empty($img)) continue;
				
				?>
				
				
				<div class="product-gallery-item slick-slide">
                	<figure>
						<amp-img src="<?php echo get_template_directory_uri() ?>/images/products/<?php echo trim($img); ?>?v=1"
							on="tap:lightbox1"
							role="button"
							tabindex="0"
							width="330"
							height="223"
							layout="fixed"
							alt="<?php echo get_the_title() . ' ' . __( 'Product image', 'myrobotcenter' ) .': ' .($img_key+1); ?>"
							class="product-gallery-image "
							>						
						</amp-img>
					<figcaption><?php the_title();?></figcaption> 
					</figure> 
						
                </div>
               
                <?php } ?>
                
			</amp-carousel>
				
			
			<!--
			<amp-selector layout=container name="carousel-prod-selector" [selected]="selectedSlideProd<?php echo $post_no; ?>"
				on="select:AMP.setState({selectedSlideProd<?php echo $post_no; ?>: event.targetOption})">
   				
				<ul class="slick-dots" role="tablist">
						
					<?php foreach ($slider_img_arr as $key => $img) { 
					
							if (empty($img)) continue;
					?>
						 
						<li>
							<button on="tap:product-gallery-<?php echo $post_no; ?>.goToSlide(index=<?php echo $key; ?>)" type="button" role="button" tabindex="0" option=<?php echo $key; ?>><?php echo ($key+1); ?></button>
						</li>
							
					<?php } ?>
							
													
							
								
				</ul>
			</amp-selector>
			-->
    
    
		<?php } ?>
	
	
		<!-- Product features. -->
	
	
        <div class="product-features">
            <div class="max-width">
                <div class="product-features-top">
                    <div class="grid">
                        <div class="grid__item one-half iphoneh--one-whole palm--one-whole">
                            <h3 class="headline-third">
                                <?php _e( 'Features', 'myrobotcenter' ); ?>                             
							</h3>
								
                            <div class="product-features-description">
                                <?php echo $features_short_description; ?> 
							</div>
                        </div>
                        <div class="grid__item one-half iphoneh--one-whole palm--one-whole">
                                <div class="text-align-right download-btn">
								
									<?php
									
									$analytics_data[] = array(
										'index' => $tracking_index,
										'action' => 'Download - PDF',
										'category' => 'Download',
										'label' => 'Download: ' . __('Download product-data-sheet', 'myrobotcenter') . ' (.pdf) ',
										
										); 
									
									
									?>
								
								
								
                                    <a id="tracking-<?php echo $tracking_index++; ?>"
                                       href="<?php echo get_template_directory_uri() ?>/pdfs/<?php echo $pdf_link; ?>?v=1"
                                       title="<?php _e('Download product-data-sheet', 'myrobotcenter'); ?>"
                                       target="_blank"
                                       class="btn is-white"
                                       data-tracking-category="Download"
                                       data-tracking-action="Download - PDF"
                                       data-tracking-label="Download: <?php _e('Download product-data-sheet', 'myrobotcenter'); ?> (.pdf)">
                                        
                                        <?php _e('Download product-data-sheet', 'myrobotcenter'); ?>                                                                        </a>
                                </div>
                                                    </div>
                    </div>
                </div>
				
				
				

                <div class="product-features-bottom">
					<?php //echo $features_bottom_block 
					
					$lang = substr(get_bloginfo("language"),0, 2);
					$data_arr = get_features_data($lang, $post_no);
										
					if (!empty($data_arr)) {
						
						set_query_var( 'data_arr', $data_arr);
						get_template_part( 'amp/template-parts/features' ); 

					}
				?>	
					
			    </div>
				
				
				
            </div>
        </div>
    
    <!-- Product video. -->
    
    <!-- Product reviews. -->
	
	<?php
	
	$lang = substr(get_bloginfo("language"),0, 2);
	$review_arr = get_slider_data($lang, $post_no);
 
 
	 //echo $lang . '- ' . $post_no;
	 //print_r($review_arr);
 
 
	if (!empty($review_arr)) {
		
		set_query_var( 'review_arr', $review_arr);
		 get_template_part( 'amp/template-parts/reviews' ); 

	}
	?>
	
	
    </article>
	
	
	