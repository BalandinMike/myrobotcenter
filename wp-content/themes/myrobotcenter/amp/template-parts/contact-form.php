

<?php $lang = substr(get_bloginfo("language"),0, 2); ?>

<section class="contact-section">
    <div class="max-width">
        <div class="contact-inner">
            <h2 class="headline-second">                
				<?php _e( 'Contact', 'myrobotcenter' ); ?>
			</h2>

            <div class="title-seperator">
                <span class="seperator"></span>
            </div>

            <div class="contact-text">                 
				<?php _e( 'Do you have questions about our products? Contact us!', 'myrobotcenter' ); ?>
			</div>

			<div class="contact-form-container">		
				<?php 
				
					//echo do_shortcode('[contact-form-7 id="48" title="Contact form"]'); 
				
					
				
				
					if ($lang == 'de')
						
						echo do_shortcode('[contact-form-7 id="48" title="Contact form"]'); 
				
					elseif ($lang == 'fr')
					
						echo do_shortcode('[contact-form-7 id="120" title="Contact form"]'); 
				
					else 
					
					echo do_shortcode('[contact-form-7 id="47" title="Contact form"]'); 
				
				
				?>			
			</div>			

            
        </div>
    </div>
</section>



