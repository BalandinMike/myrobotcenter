
	<div class="grid">
					
		<?php	foreach($data_arr as $item) { ?> 
					
                <div class="grid__item one-half iphoneh--one-whole palm--one-whole">
                    <div class="grid">
                        <div class="grid__item two-twelfths">
                            <div class="text-align-right">
                                           										   
								<amp-img src="<?php echo get_template_directory_uri() .'/' .$item['image'] ; ?>"
									width="60"
									class="product-feature-image"
									layout="responsive"
									alt="<?php echo $item['title']; ?>"
									height="60">						
								</amp-img>
										
										
							</div>
                        </div>
                        <div class="grid__item ten-twelfths">
                            <div class="product-feature-content">
                                <h4 class="headline-fourth">
									<?php echo $item['title']; ?>
                                </h4>

                                <div class="product-feature-text">
									<?php echo $item['text']; ?>
                                                                                            
								</div>
                            </div>
                        </div>
                    </div>
                </div>
						
				<?php } ?>
					
    </div>