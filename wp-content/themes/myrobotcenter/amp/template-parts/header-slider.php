<?php


?>

<section class="header-slider-section clearfix js-header-slider">
    <div id="header-slider-gradient" class="header-slider-gradient" ></div>

	
		<amp-carousel 
		  id="carousel-header-slider"
		  class="header-slider"
		  width="1349"
		  height="562"
		  layout="responsive"
		  type="slides"	  
		  autoplay delay="2000" 
		  [slide]="selectedSlide" 
		  on="slideChange:AMP.setState({selectedSlide: event.index})">
		  			
	         <div class="header-slider-item">
					
					
					<amp-img src="<?php echo get_template_directory_uri() ?>/images/title-slider/title-image-01.png?v=1"
						width="1349"
						height="562"
						layout="responsive"
						alt="Die Roboter der Eigenmarke von myRobotcenter">
					</amp-img>	
								
					<div class="header-slider-text-container">
						<div class="header-slider-title"> 
							<?php _e('Life is too short for bothersome housework.', 'myrobotcenter'); ?>                                        
						</div>

						<div class="header-slider-text">
							<?php _e('Intelligent household robots with innovative features.', 'myrobotcenter'); ?>                                        
						</div>
					</div>
			</div>
				
			<div class="header-slider-item">
					
					
					<amp-img src="<?php echo get_template_directory_uri() ?>/images/title-slider/title-image-05.png?v=1"
						width="1349"
						height="562"
						layout="responsive"
						alt="Das Leben ist zu kurz für lästige Hausarbeiten.">
					</amp-img>
					
					
					<div class="header-slider-text-container">
						<div class="header-slider-title">
							<?php _e('myVacBot SN500 Robot Vacuum', 'myrobotcenter'); ?>                                        
						</div>

						<div class="header-slider-text">
							 <?php _e('The Robot Vacuum with intelligent Robart Navigation Technology.', 'myrobotcenter'); ?>                                           
						</div>
						<div id="anchor-link-4">
							<a href="#product-item-anchor-1" title="<?php _e('Find out more', 'myrobotcenter'); ?>" class="btn header-slider-button">
								<?php _e('Find out more', 'myrobotcenter'); ?>                                                
							</a>
						</div>

					</div>
			</div>
				
				<div class="header-slider-item">
					
					<amp-img src="<?php echo get_template_directory_uri() ?>/images/title-slider/title-image-02.png?v=1"
						width="1349"
						height="562"
						layout="responsive"
						alt="myVacBot SN500 Staubsauger Roboter">
					</amp-img>
					
					
					
					<div class="header-slider-text-container">
						<div class="header-slider-title">
							<?php _e('myVacBot S200 Vacuum and Floor Mopping Robot', 'myrobotcenter'); ?>                                         
						</div>

						<div class="header-slider-text">
							<?php _e('The smart combo-talent for vacuuming and mopping work.', 'myrobotcenter'); ?>                                         
						</div>

						<div id="anchor-link-5">																													
							<a href="#product-item-anchor-2" title="<?php _e('Find out more', 'myrobotcenter'); ?>" class="btn header-slider-button">
								<?php _e('Find out more', 'myrobotcenter'); ?>                                                
							</a>
						</div>
					</div>
				</div>
				
				
				<div class="header-slider-item">
										
					<amp-img src="<?php echo get_template_directory_uri() ?>/images/title-slider/title-image-03.png?v=1"
						width="1349"
						height="562"
						layout="responsive"
						alt="myVacBot S200 Saug und Wischroboter">
					</amp-img>
					
					
					
					<div class="header-slider-text-container">
					<div class="header-slider-title">
						<?php _e('myVacBot B100 Vacuum Robot', 'myrobotcenter'); ?>                                         
					</div>

					<div class="header-slider-text">
						<?php _e('The innovative household helper with new brush design.', 'myrobotcenter'); ?>                                         
					</div>

					<div id="anchor-link-6">	
						<a href="#product-item-anchor-3" title="<?php _e('Find out more', 'myrobotcenter'); ?>" class="btn header-slider-button">
							<?php _e('Find out more', 'myrobotcenter'); ?>                                                
						</a>
					</div>
																																</div>
				</div>
				
				
				<div class="header-slider-item">
					
					<amp-img src="<?php echo get_template_directory_uri() ?>/images/title-slider/title-image-04.png?v=1"
						width="1349"
						height="562"
						layout="responsive"
						alt="Der myVacBot B100 Saugroboter">
					</amp-img>
					
					
					
					<div class="header-slider-text-container">
						<div class="header-slider-title">
							<?php _e('The robots of the Home Brand of myRobotcenter', 'myrobotcenter'); ?>                                       
						</div>

						<div class="header-slider-text">
							<?php _e('Quality at a convincing Price-Performance Ratio.', 'myrobotcenter'); ?>                                           
						</div>

						
					</div>
				</div>
				
			
	
	</amp-carousel>
	
	
	<amp-selector layout=container name="carousel-selector" [selected]="selectedSlide"
      on="select:AMP.setState({selectedSlide: event.targetOption})">
   				
	<ul class="slick-dots" role="tablist">
		<li>
			<button type="button" role="button" tabindex="0" option=0>1</button>
		</li>
		
		<li>
			<button type="button" role="button" tabindex="1" option=1>2</button>
		</li>
		
		<li>
			<button type="button" role="button" tabindex="2" option=2>3</button>
		</li>
		
		<li>
			<button type="button" role="button" tabindex="2" option=3>4</button>
		</li>
		
		<li>
			<button type="button" role="button" tabindex="2" option=4>5</button>
		</li>
		
		
			
	</ul>
	</amp-selector>
        
	
</section>