 
 <?php 
 
 
 ?>
 
 
 <div class="product-reviews">
            <div class="product-review-overlay"></div>

            <span class="is-hidden"
                  itemprop="aggregateRating"
                  itemscope
                  itemtype="http://schema.org/AggregateRating">
                <meta itemprop="ratingValue" content="4.3"/>
                <meta itemprop="reviewCount" content="3"/>
            </span>

            <div class="max-width">
                <div class="product-reviews-inner">
                    <h3 class="headline-third"><?php _e('Reviews', 'myrobotcenter'); ?></h3>

                    <div class="product-reviews-title-seperator">
                        <span class="seperator"></span>
                    </div>

                    <div class="product-reviews-text">                         
						<?php _e('What do customers think about this product?', 'myrobotcenter'); ?>	
					</div>

                    
					<amp-carousel 
					  id="carousel-feature-slider-<?php echo $post_no; ?>"
					  width="600"
					  height="234"
					  layout="responsive"
					  type="slides"	
					  class="product-reviews-slider"
					  autoplay delay="2000" loop
					  [slide]="selectedFeatureSlide<?php echo $post_no; ?>" 
					  on="slideChange:AMP.setState({selectedFeatureSlide<?php echo $post_no; ?>: event.index})">
					  




					
							<?php foreach ($review_arr as $review) { ?>
						 
                            <div class="product-reviews-item" itemprop="review" itemscope itemtype="https://schema.org/Review">
                                <div class="grid grid--middle grid--rev">
                                    <div class="grid__item four-fifths special--one-whole palm--one-whole">
                                        <div class="bubble">
                                            <div class="top-container">
                                                <div class="quotation" itemprop="description">
                                                    &bdquo;<?php echo $review['text'] ?>&ldquo;
                                                </div>
                                            </div>

                                            <div class="bottom-container">
                                                <div class="user-info">
                                                    <meta itemprop="author"
                                                          content="<?php echo $review['author'] ?>"/>
                                                    &minus;&nbsp;<?php echo $review['author'] ?>
												</div>
                                                <div class="rating"
                                                     itemprop="reviewRating"
                                                     itemscope
                                                     itemtype="https://schema.org/Rating">
                                                    
                                                    <meta itemprop="ratingValue" content="<?php echo $review['ratingValue'] ?>"/>

                                                        <i class="icon-star <?php echo rating_is_star_filled(1, $review['ratingValue'] ); ?>"></i>
                                                        <i class="icon-star <?php echo rating_is_star_filled(2, $review['ratingValue'] ); ?>"></i>
                                                        <i class="icon-star <?php echo rating_is_star_filled(3, $review['ratingValue'] ); ?>"></i>
                                                        <i class="icon-star <?php echo rating_is_star_filled(4, $review['ratingValue'] ); ?>"></i>
                                                        <i class="icon-star <?php echo rating_is_star_filled(5, $review['ratingValue'] ); ?>"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="grid__item one-fifth special--one-whole palm--one-whole">
                                        <div class="user-img-container">
                                            
											<amp-img src="<?php echo get_template_directory_uri() ?>/images/reviews/<?php echo $review['image'] ?>"
												width="100"
												height="100"
												class="user-img"
												layout="responsive">
											</amp-img>
											
											
											
                                        </div>
                                    </div>
                                </div>
                            </div>
							
							<?php } ?>
							
						
					</amp-carousel>	


					<amp-selector layout=container name="carousel-selector" [selected]="selectedFeatureSlide<?php echo $post_no; ?>"
						on="select:AMP.setState({selectedFeatureSlide<?php echo $post_no; ?>: event.targetOption})">
   				
						<ul class="slick-dots" role="tablist">
						
							<?php foreach ($review_arr as $key => $review) { ?>
						 
							<li>
								<button type="button" role="button" tabindex="0" option=<?php echo $key; ?>><?php echo ($key+1); ?></button>
							</li>
							
							<?php } ?>
							
													
							
								
						</ul>
						</amp-selector>
						
                    
                </div>
            </div>
</div>