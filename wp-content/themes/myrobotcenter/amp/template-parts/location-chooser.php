<?php

global $lang;


?>

    <div class="location-chooser-title">                
		<?php _e( 'Please select your country', 'myrobotcenter' ); ?>				
			
			
	</div>

    <ul>           
	<?php 
				
		if (function_exists('qtranxf_getSortedLanguages')) {
						
			global $q_config;
			//print_r(qtranxf_getSortedLanguages());
			foreach(get_language() as $language) {
							
				$lang_url = qtranxf_convertURL('', $language, false, true);
				$lang_label = $q_config['language_name'][$language];
							
			?>	
						
				<li>
								<a href="<?php echo $lang_url; ?>"
								   title="<?php echo $lang_label; ?>"
								   class="location-chooser-item">
									<span class="location-icon">
										<i class="flag-icon <?php echo $language; ?>"></i>
									</span>
									<span class="select-text">
										<?php echo $lang_label; ?>                       </span>
								</a>
				</li>
							
		<?php	
							
			}
						
						
						
		}?>
				
    </ul>         
            
		