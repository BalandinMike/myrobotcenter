<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage MyRobotCenter
 * @since MyRobotCenter 1.0
 */
?>
		
	<footer class="footer" itemscope itemtype="https://schema.org/Organization">
    
	<div class="max-width">
        <ul class="footer-list" itemscope itemtype="https://schema.org/PostalAddress">
            <li itemprop="name">myRobotcenter GmbH</li>
            <li itemprop="streetAddress"><?php _e( 'streetAddress', 'myrobotcenter' ); ?></li>
            <li itemprop="postalCode"><?php _e( 'PostalCode', 'myrobotcenter' ); ?></li>
            <li itemprop="addressLocality"><?php _e( 'AddressLocality', 'myrobotcenter' ); ?></li>
            <li itemprop="email"><?php _e( 'email', 'myrobotcenter' ); ?></li>
            <li>www.myrobotcenter.com</li>
            <li itemprop="telephone">00800 500 500 10</li>
        </ul>

        <ul class="footer-nav-list">
            <li><a href="https://<?php _e( 'gtc_url', 'myrobotcenter' ); ?>/" target="_blank"><?php _e( 'GTC', 'myrobotcenter' ); ?></a></li>
            <li><a href="https://<?php _e( 'legal_information_url', 'myrobotcenter' ); ?>/" target="_blank"><?php _e( 'Legal information', 'myrobotcenter' ); ?></a></li>
            <li><a href="https://<?php _e( 'privacy_policy_url', 'myrobotcenter' ); ?>/" target="_blank"><?php _e( 'Privacy Policy', 'myrobotcenter' ); ?></a></li>
            <li><a href="https://<?php _e( 'right_to_cancel_url', 'myrobotcenter' ); ?>/" target="_blank"><?php _e( 'Right to cancel', 'myrobotcenter' ); ?></a></li>
        </ul>
    </div>
	
	
</footer>

