<?php
/**
 * MyRobotCenter functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage MyRobotCenter
 * @since MyRobotCenter 1.0
 */


if ( ! function_exists( 'myrobotcenter_setup' ) ) :

function myrobotcenter_setup() {
	
	load_theme_textdomain( 'myrobotcenter' );

	
	set_post_thumbnail_size( 1200, 9999 );

	
	

}
endif; 
add_action( 'after_setup_theme', 'myrobotcenter_setup' );


//if (function_exists('register_nav_menu'))
//{
//    register_nav_menu('header_menu',  _e( 'Header Menu', 'twentysixteen' ));
//}

function mrc_top_menu() {
  register_nav_menu('mrc-top-menu',__( 'Header Menu' ));
}
add_action( 'init', 'mrc_top_menu' );


add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
	
	$classes[] = "anchor-item";
	 
	 return $classes;
}




function myrobotcenter_scripts() {
	
	
	// Theme stylesheet.
	wp_enqueue_style( 'myrobotcenter-style', get_stylesheet_uri() );
	wp_enqueue_style( 'myrobotcenter-style-base', get_template_directory_uri() . '/css/style-base.css', array(), '0.0.1');
	//wp_enqueue_style( 'myrobotcenter-style-fonts', get_template_directory_uri() . '/css/fonts.css', array(), '0.0.1');

	
	//wp_enqueue_script( 'myrobotcenter-js', get_template_directory_uri() . '/js/script.min.js', array(), '0.0.1', true );

	
}
add_action( 'wp_enqueue_scripts', 'myrobotcenter_scripts' );


function get_slider_data($lang, $slider_no) {
	
	$review_arr = array(
		
		
		'2' => array(
	
			'en' => array(
				array(	'author' => 'Paul Smith',
						'ratingValue' => 4,
						'image' => 'user-1-en.jpg',
						'text' => 'Top device - maintains what it promises. What I love most is its App. That’s how I can easily and rapidly operate the device.'
						),
				array(	'author' => 'Ian Brown',
						'ratingValue' => 4,
						'image' => 'user-3-en.jpg',
						'text' => 'Clean floors at the push of a button? Yes, it is really true. Honestly, I have been sceptical at the beginning but now I am really  happy with the robot.'
						),	
				array(	'author' => 'Jozef & Maya Riihimäki',
						'ratingValue' => 5,
						'image' => 'user-4-en.jpg',
						'text' => 'We are absolutely happy with the robot vacuum. It reliably starts at the pre-programmed time and the App is really easy to handle.'
						),	
			),
			
			
			
			'de' => array(
				array(	'author' => 'Christian Müller',
						'ratingValue' => 4,
						'image' => 'user-1-de.jpg',
						'text' => 'Super Gerät – hält was er verspricht. Besonders praktisch finde ich die App. Hier kann ich den Saugroboter einfach und sehr schnell bedienen.'
						),
				array(	'author' => 'Friedrich Köpke',
						'ratingValue' => 4,
						'image' => 'user-6-de.jpg',
						'text' => 'Saubere Böden auf Knopfdruck? Ja so ist es wirklich. Ich war anfangs sehr skeptisch, aber jetzt bin ich total zufrieden mit dem Roboter.'
						),	
				array(	'author' => 'Paul und Anna Huber',
						'ratingValue' => 5,
						'image' => 'user-2-de.png',
						'text' => 'Wir sind absolut zufrieden mit dem Gerät. Es startet immer pünktlich mit der Reinigung und auch die App ist sehr einfach zu bedienen.'
						),	
			),
			
			
			'fr' => array(
				array(	'author' => 'Valentin Durand',
						'ratingValue' => 4,
						'image' => 'user-1-fr.jpg',
						'text' => "Un super appareil – il apporte ce qu'il promet. Je trouve l'application particulièrement pratique. Elle me permet d'utiliser rapidement et facilement le robot aspirateur."
						),
				array(	'author' => 'Olivier Legrand',
						'ratingValue' => 4,
						'image' => 'user-2-fr.jpg',
						'text' => "Des sols propres par simple pression d'un bouton? Oui, c'est vrai. Très sceptique au début, je suis maintenant absolument satisfait."
						),	
				array(	'author' => 'Paul et Amélie Perrin',
						'ratingValue' => 5,
						'image' => 'user-3-fr.jpg',
						'text' => "Nous sommes absolument satisfaits de cet appareil. Il commence le nettoyage toujours à l'heure et je n'ai aucune difficulté à utiliser l'application."
						),	
			),
			
			
		),


		'3' => array(
	
			'en' => array(
				array(	'author' => 'Family Miller',
						'ratingValue' => 4,
						'image' => 'user-5-en.jpg',
						'text' => 'We can really recommend the robot vacuum. It cleans thoroughly, starts at the programmed time and even cleans our carpets!'
						),
				array(	'author' => 'Andrea Lloyd',
						'ratingValue' => 5,
						'image' => 'user-6-en.jpg',
						'text' => 'Top vacuum robot at an unbeatable price-performance ratio. It absolutely meets my challenges and I am totally happy with it.'
						),	
				array(	'author' => 'Graham & Vivien Walker',
						'ratingValue' => 4,
						'image' => 'user-2-en.png',
						'text' => 'We are really happy with the robot. Its price-performance ration is just perfect and it cleans thoroughly and effortlessly.'
						),	
			),
			
			
			'de' => array(
				array(	'author' => 'Oma Käti',
						'ratingValue' => 4,
						'image' => 'user-4-de.jpg',
						'text' => 'Den Saugroboter kann ich wirklich empfehlen. Er reinigt gründlich, startet zur programmierten Zeit und saugt sogar meine Teppiche.'
						),
				array(	'author' => 'Johanna Stöckler',
						'ratingValue' => 5,
						'image' => 'user-3-de.jpg',
						'text' => 'Top Saugroboter zu unschlagbarem Preis-Leistungsverhältnis. Er wird meinen Anforderungen gerecht und ich bin sehr zufrieden.'
						),	
				array(	'author' => 'Markus und Elisabeth Birkmayr',
						'ratingValue' => 4,
						'image' => 'user-5-de.jpg',
						'text' => 'Wir sind mit dem Saugroboter sehr zufrieden. Er saugt gründlich und trotz der ganzen Konkurrenz ist das Preis-Leistungs-Verhältnis sehr gut.'
						),	
			),
			
			
			
			
			
			'fr' => array(
				array(	'author' => 'Mme. Ophélie',
						'ratingValue' => 4,
						'image' => 'user-4-fr.jpg',
						'text' => "Je recommande vraiment l'utilisation du robot aspirateur. Il nettoie en profondeur même mes moquettes et commence toujours son travail à l'heure programmée."
						),
				array(	'author' => 'Pauline Chevalier',
						'ratingValue' => 5,
						'image' => 'user-5-fr.jpg',
						'text' => "Un robot aspirateur en or à un prix imbattable. Il répond complètement à mes exigences et je suis tout à fait satisfait."
						),	
				array(	'author' => 'Sébastien et Sophie Girard',
						'ratingValue' => 4,
						'image' => 'user-6-fr.jpg',
						'text' => "Nous sommes vraiment satisfaits de ce robot aspirateur. Il nettoie en profondeur : un rapport qualité-prix irréprochable."
						),	
			),
			
		),
		
	);
	
	if (!empty($review_arr[$slider_no][$lang]))
		return $review_arr[$slider_no][$lang];
	else 
		return array();
	
	
}

function rating_is_star_filled($star_no, $rating ) {
	
	if ($star_no <= $rating)
		return 'filled';
	else return '';
	
}



function get_features_data($lang, $product_no) {
	
	$data_arr = array(
		
		
		'1' => array(
	
			'en' => array(
				array(	'image' => 'images/robart.png',
						'title' => 'Robart Navigation Technology',
						'text' => 'With the help of the Austrian Robart Navigation Technology the myVacBot SN500 robot vacuum takes care of a thorough floor cleaning.'
						),
				array(	'image' => 'images/app.png',
						'title' => 'App Control',
						'text' => 'An App makes operating the myVacBot SN500 robot vacuum even more comfortable. Start, stop, programme – all via Smartphone at the push of a button.'
						),
				array(	'image' => 'images/timer.png',
						'title' => 'Time Programming',
						'text' => 'Once programmed, the myVacBot SN500 robot vacuum reliably carries out its cleaning tasks – even when you are not at home yourself.'
						),
				array(	'image' => 'images/battery.png',
						'title' => 'Lithium-Ion Battery',
						'text' => 'With its powerful Lithium-Ion battery the myVacBot SN500 robot vacuum effortlessly cleans floors with up to 120m<sup>2</sup>.'
						),
				array(	'image' => 'images/two-brushes.png',
						'title' => 'Two Brush-System',
						'text' => 'With the help of two different floor brushes the myVacBot SN500 robot vacuum thoroughly removes dust and other pollution from hard floors.'
						),
			),
			
			
			
			'de' => array(	
			
				array(	'image' => 'images/robart.png',
						'title' => 'Navigations-Technologie',
						'text' => 'Mit der in Österreich entwickelten Robart Navigations-Technologie sorgt der myVacBot SN500 Staubsauger Roboter für eine lückenlose Bodenreinigung.'
					),
				array(	'image' => 'images/app.png',
						'title' => 'App Steuerung',
						'text' => 'Eine App macht die Steuerung des myVacBot SN500 Staubsauger Roboters noch bequemer. Starten, stoppen, programmieren – alles bequem auf Knopfdruck via Smartphone erledigen.'
						),
				array(	'image' => 'images/timer.png',
						'title' => 'Zeitprogrammierung',
						'text' => 'Wurde der myVacBot SN500 Staubsauger Roboter einmal programmiert, führt er zuverlässig seine Reinigungsaufträge aus. Selbst wenn Sie nicht zu Hause sind.'
						),
				array(	'image' => 'images/battery.png',
						'title' => 'Lithium-Ionen Akku',
						'text' => 'Mit seinem leistungsstarken Lithium-Ionen Akku reinigt der myVacBot SN500 Flächen mit bis zu 120m<sup>2</sup>.'
						),
				array(	'image' => 'images/two-brushes.png',
						'title' => 'Zwei Bürsten-System',
						'text' => 'Mithilfe von zwei unterschiedlichen Bodenbürsten entfernt der myVacBot SN500 Staubsauger Roboter gründlich Staub und andere Verschmutzungen.'
						),
			
			),
			
			
			'fr' => array(
			
				array(	'image' => 'images/robart.png',
						'title' => 'Technologie de navigation Robart',
						'text' => 'Grâce à la technologie de navigation Robart, développée en Autriche, le robot aspirateur nettoie parfaitement et en profondeur.'
					),
				array(	'image' => 'images/app.png',
						'title' => 'Commande à distance par application',
						'text' => "Vous pouvez le démarrer, le stopper, le programmer et le surveiller très facilement grâce à l'application gratuite pour Android et iOS."
						),
				array(	'image' => 'images/timer.png',
						'title' => 'Fonction de programmation',
						'text' => "Une fois programmé, le myVacBot nettoie en toute fiabilité même si vous n'êtes pas présent."
						),
				array(	'image' => 'images/battery.png',
						'title' => 'Batterie Lithium-Ion',
						'text' => "L'autonomie du myVacBot SN500 lui permet de nettoyer jusqu'à 120 m<sup>2</sup> de sols durs et de moquette grâce à sa batterie Lithium-Ion puissante."
						),
				array(	'image' => 'images/two-brushes.png',
						'title' => 'Système à deux brosses',
						'text' => "Grâce aux deux brosses distinctes, le robot aspirateur élimine la poussière et autres impuretés en profondeur."
						),
			
			
			),
			
			
		),

		// -------- 2
		
		'2' => array(
	
			'en' => array(
				array(	'image' => 'images/app.png',
						'title' => 'App-Controlling',
						'text' => 'The myVacBot S200 vacuum and floor mopping robot can comfortably be controlled via Smartphone. Start, stop, programme and monitor the vacuum robot - from wherever you are!'
						),
				array(	'image' => 'images/microfiber.png',
						'title' => 'Rotating Microfiber Pad',
						'text' => 'The myVacBot S200 cannot only vacuum but also floor mop your floors. Its rotating Microfiber Pad removes even stubborn pollution.'
						),
				array(	'image' => 'images/timer.png',
						'title' => 'Time Programming',
						'text' => 'Once programmed, the myVacBot S200 vacuum and floor mopping robot reliably carries out its cleaning tasks. There can be set one starting time each weekday.'
						),
				array(	'image' => 'images/battery.png',
						'title' => 'Lithium-Ion Battery',
						'text' => 'With its powerful Lithium-Ion battery the myVacBot S200 vacuum and floor mopping robot is able to efortlessly handle areas with up to 150m<sup>2</sup>.'
						),
				array(	'image' => 'images/sonicwall.png',
						'title' => 'Limiting Areas',
						'text' => 'With the help of the Sonic Wall you can set limits to the cleaning area from myVacBot S200 vacuum and floor mopping robot. Furthermore it builds a valuable boundary near abysses and staircases.'
						),
				array(	'image' => 'images/collision.png',
						'title' => 'Avoiding Collision',
						'text' => 'The “Soft Touch” function avoids collision of myVacBot S200 vacuum and floor mopping robot with obstacles. The function can optionally be activated.'
						),		
						
			),
			
			
			
			'de' => array(	
			
				
				array(	'image' => 'images/app.png',
						'title' => 'App Steuerung',
						'text' => 'Dank App-Steuerung bietet der myVacBot S200 Saug und Wischroboter noch mehr Komfort. Via App kann der Saug und Wischroboter gestartet, gestoppt, programmiert und überwacht werden - wo auch immer Sie gerade sind.'
						),
				array(	'image' => 'images/microfiber.png',
						'title' => 'Rotierendes Mikrofasertuch',
						'text' => 'Der myVacBot S200 kann nicht nur saugen sondern auch wischen. Sein rotierendes Mikrofasertuch entfernt selbst hartnäckige Verschmutzungen.'
						),
				array(	'image' => 'images/timer.png',
						'title' => 'Zeitsteuerung',
						'text' => 'Einmal programmiert führt der myVacBot S200 Saug und Wischroboter zuverlässig seine Reinigungsaufträge aus. Pro Wochentag kann eine Startzeit programmiert werden.'
						),
				array(	'image' => 'images/battery.png',
						'title' => 'Lithium-Ionen-Akku',
						'text' => 'Der myVacBot S200 Saug und Wischroboter ist mit einem leistungsstarken Lithium-Ionen-Akku ausgestattet. Flächen mit bis zu 150m<sup>2</sup> werden deshalb problemlos gemeistert.'
						),
				array(	'image' => 'images/sonicwall.png',
						'title' => 'Eingrenzung von Flächen',
						'text' => 'Mit der sogenannten „Sonic Wall“ kann die Reinigungsfläche des myVacBot S200 Saug und Wischroboters mühelos eingegrenzt werden. Die „Sonic Wall“ bildet zugleich eine wertvolle Absturzsicherung.'
						),
				array(	'image' => 'images/collision.png',
						'title' => 'Schutz vor Kollision',
						'text' => 'Die „Soft Touch“ Funktion verhindert, dass der myVacBot S200 Saug und Wischroboter mit Hindernissen kollidiert. Die Funktion kann optional aktiviert werden.'
						),	
			
			),
			
			
			'fr' => array(
			
				
				array(	'image' => 'images/app.png',
						'title' => 'Commande par application',
						'text' => 'L\'application pour <a href="https://play.google.com/store/apps/details?id=com.lancens.qq6wMyVacBot" target="_blank">Android</a> et <a href="https://itunes.apple.com/fr/app/myvacbot/id1175974274?mt=8" target="_blank">iOS</a> permet de commander le robot aspirateur S200 depuis un smartphone ou une tablette.'
						),
				array(	'image' => 'images/microfiber.png',
						'title' => 'Aspirer et laver',
						'text' => "Le myVacBot S200 est aussi bien aspirateur que laveur. Il est équipé d'une serpillère en microfibres rotative qui lui permet d'éliminer les taches les plus tenaces."
						),
				array(	'image' => 'images/timer.png',
						'title' => 'Fonction de programmation',
						'text' => "La fonction de programmation permet de démarrer le myVacBot à l'heure voulue afin qu'il nettoie de façon fiable. Il est possible de définir un horaire de démarrage différent pour chaque jour de la semaine."
						),
				array(	'image' => 'images/battery.png',
						'title' => 'Batterie Lithium-Ion',
						'text' => "Grâce à sa puissante batterie Lithium-Ion, le robot aspirateur et laveur dispose d'une autonomie lui permettant de nettoyer jusqu'à 150m<sup>2</sup> de sols durs ou de moquette"
						),
				array(	'image' => 'images/sonicwall.png',
						'title' => 'Délimiter les zones à nettoyer',
						'text' => "Le Sonic Wall est une limite invisible stoppant le robot aspirateur et laveur. Il prévient donc parfaitement les chutes dans les escaliers. "
						),
				array(	'image' => 'images/collision.png',
						'title' => 'Détecte les obstacles',
						'text' => "La fonction “Soft-Touch“ protège le robot aspirateur des collisions avec des obstacles tels que les meubles, les murs ou autres."
						),	
			
			
			),
			
			
		),
		// --------- 3
		
		'3' => array(
	
			'en' => array(
				array(	'image' => 'images/brush.png',
						'title' => 'Innovative Brush Arrangement',
						'text' => 'The V-shaped brush arrangement offers an even bigger absorption surface to ensure an extraordinarily thorough cleaning.'
						),
				array(	'image' => 'images/uv.png',
						'title' => 'Antiseptic UV-Light',
						'text' => 'The integrated UV-Light takes care of killing bacteria, virus and other microorganisms. Therefore the myVacBot B100 vacuum robot is especially suitable for allergy sufferers.'
						),
				array(	'image' => 'images/timer.png',
						'title' => 'Time Programming',
						'text' => 'The myVacBot B100 vacuum robot is able to independently start cleaning at a pre-programmed time. Once programmed, it will clean your floors reliably.'
						),
				array(	'image' => 'images/sonicwall.png',
						'title' => 'Limiting Areas',
						'text' => 'By using the Sonic Wall one can limit the cleaning area. Additionally it builds a useful boundary when myVacBot B100 vacuum robot is operated near abysses and staircases.'
						),
										
				array(	'image' => 'images/collision.png',
						'title' => 'Avoiding Collision',
						'text' => 'In “Soft Touch” function the myVacBot B100 vacuum robot is able to avoid colliding with obstacles. This function can be activated individually.'
						),		
						
				array(	'image' => 'images/cleaningmodes.png',
						'title' => 'Different Cleaning Modes',
						'text' => 'The myVacBot B100 vacuum robot is equipped with five cleaning modes. They can either be operated separately or by combining all modes (Auto-Mode).'
						),		
						
			),
			
			'de' => array(
				array(	'image' => 'images/brush.png',
						'title' => 'Innovative Anordnung der Bürsten',
						'text' => 'Die V-förmige Anordnung der Bürsten bieten noch mehr Aufnahmefläche und stellen eine besonders gründliche Reinigung sicher.'
						),
				array(	'image' => 'images/uv.png',
						'title' => 'Keimtötendes UV-Licht',
						'text' => 'Das integrierte UV-Licht tötet Viren, Bakterien und andere Mikroorganismen ab. Der myVacBot B100 Saugroboter ist deshalb besonders für Allergiker geeignet.'
						),
				array(	'image' => 'images/timer.png',
						'title' => 'Zeitsteuerung',
						'text' => 'Einmal programmiert, erledigt der myVacBot B100 Saugroboter zuverlässig seine vorprogrammierten Reinigungsaufträge. Es kann pro Wochentag eine Startzeit hinterlegt werden.'
						),
				array(	'image' => 'images/sonicwall.png',
						'title' => 'Eingrenzung von Flächen',
						'text' => 'Mit der „Sonic Wall“ kann nicht nur die Reinigungsfläche des myVacBot B100 Saugroboters eingegrenzt werden - sie bietet zusätzlich eine wertvolle Absturzsicherung an Gefahrenstellen.'
						),
										
				array(	'image' => 'images/collision.png',
						'title' => 'Schutz vor Kollision',
						'text' => 'Der „Soft Touch“ Modus kann optional aktiviert werden und verhindert, dass der myVacBot B100 Saugroboter mit Hindernissen kollidiert.'
						),		
						
				array(	'image' => 'images/cleaningmodes.png',
						'title' => 'Verschiedene Reinigungsmodi',
						'text' => 'Mit fünf unterschiedlichen Reinigungsmodi kümmert sich der myVacBot B100 Saugroboter um eine flächendeckende Bodenreinigung.'
						),		
						
			),
			
			'fr' => array(
				array(	'image' => 'images/brush.png',
						'title' => 'Nouvelle disposition des brosses',
						'text' => 'Les brosses disposées en V peuvent nettoyer une surface plus étendue et garantissent un nettoyage en profondeur.'
						),
				array(	'image' => 'images/uv.png',
						'title' => "Idéal pour les personnes souffrant d'allergies",
						'text' => "La lampe UV intégrée élimine les virus, bactéries et autres microorganismes. C'est la raison pour laquelle le myVacBot B100 est le robot aspirateur idéal pour les personnes souffrant d'allergies."
						),
				array(	'image' => 'images/timer.png',
						'title' => 'Fonction de programmation',
						'text' => "Grâce à la fonction de programmation, le robot aspirateur commence son travail à l'heure désirée. Après avoir effectué le nettoyage, il rejoint sa station de chargement."
						),
				array(	'image' => 'images/sonicwall.png',
						'title' => 'Délimiter les zones à nettoyer',
						'text' => "Le «Sonic Wall» permet non seulement de délimiter la surface à nettoyer, mais il protège également l'appareil d'éventuelles chutes dans les zones dangereuses comme les escaliers p.ex."
						),
										
				array(	'image' => 'images/collision.png',
						'title' => 'Détecte les obstacles',
						'text' => "Vous pouvez activer la fonction «Soft Touch» qui fait en sorte que le robot aspirateur nettoie sans entrer en collision avec les éventuels obstacles. Le robot aspirateur détecte à temps les obstacles comme les meubles."
						),		
						
				array(	'image' => 'images/cleaningmodes.png',
						'title' => 'Nettoie les sols durs et les moquettes',
						'text' => "Le robot aspirateur offre cinq modes différents de nettoyage aussi bien pour les sols durs que pour les moquettes."
						),		
						
			),
			
		),
	);
	
	if (!empty($data_arr[$product_no][$lang]))
		return $data_arr[$product_no][$lang];
	else 
		return array();
	
	
}


function get_canonical() {
	
	global $q_config;
	
	foreach($q_config['enabled_languages'] as $lang) {
		if(!empty($q_config['locale_html'][$lang])){
			$hreflang = $q_config['locale_html'][$lang];
		}else{
			$hreflang = $lang;
		}
		echo '<link hreflang="'.$hreflang.'" href="'.qtranxf_convertURL('',$lang,false,true).'" rel="alternate" />'.PHP_EOL;
	}
	echo '<link hreflang="x-default" href="'.qtranxf_convertURL('',$q_config['default_language']).'" rel="alternate" />'.PHP_EOL;

	echo '<link rel="canonical" href="'.qtranxf_convertURL('',$q_config['language']).'"/>'.PHP_EOL;

}


function get_language() {
	
	return array('at','de','ch','en','eu','fr');
	
	
}

