<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage MyRobotCenter
 * @since MyRobotCenter 1.0
 */

	global $q_config;
 
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title><?php bloginfo( 'name' ); ?></title>
	<meta name="description" content="<?php bloginfo( 'description' ); ?>"/>
	<meta name="keywords" content="<?php _e( 'meta_keywords', 'myrobotcenter' ); ?>"/>
	
	
	<!-- BROWSER META TAGS -->
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
	<meta name="mobile-web-app-capable" content="yes"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		
	
	<!-- CUSTOM META TAGS -->
	<meta name="language" content="<?php echo $q_config['language']; ?>"/>
	<meta name="publisher" content="<?php _e( 'MASSIVE ART WebServices GmbH', 'myrobotcenter' ); ?>"/>
	<meta name="author" content="<?php _e( 'myRobotcenter GmbH', 'myrobotcenter' ); ?>"/>
	<meta name="copyright" content="<?php _e( 'myRobotcenter GmbH', 'myrobotcenter' ); ?>"/>
	<meta name="audience" content="all"/>
	<meta name="distribution" content="global"/>
	<meta name="image" content="<?php  echo get_template_directory_uri() ?>/images/metaimage.jpg"/>

	
	<!-- OG TAGS -->
	<meta property="og:site_name" content="<?php _e( 'myRobotcenter GmbH', 'myrobotcenter' ); ?>"/>
	<meta property="og:url" content="/"/>
	<meta property="og:title" content="<?php bloginfo( 'name' ); ?>"/>
	<meta property="og:type" content="website"/>
	<meta property="og:image" content="<?php  echo get_template_directory_uri() ?>/images/metaimage.jpg"/>
	<meta property="og:description" content="<?php bloginfo( 'description' ); ?>"/>
	<meta property="og:locale" content="<?php echo $q_config['language']; ?>"/>
		
	
	<!-- TWITTER TAGS -->
	<meta name="twitter:card" content="summary"/>
	<meta name="twitter:url" content="/"/>
	<meta name="twitter:title" content="<?php bloginfo( 'name' ); ?>"/>
	<meta name="twitter:description" content="<?php bloginfo( 'description' ); ?>"/>
	<meta name="twitter:image" content="<?php  echo get_template_directory_uri() ?>/images/metaimage.jpg"/>
			
		
	<!-- DC TAGS -->
	<meta name="DC.Title" content="<?php _e( 'myRobotcenter GmbH', 'myrobotcenter' ); ?>"/>
	<meta name="DC.Publisher" content="<?php _e( 'MASSIVE ART WebServices GmbH', 'myrobotcenter' ); ?>"/>
	<meta name="DC.Copyright" content="<?php _e( 'myRobotcenter GmbH', 'myrobotcenter' ); ?>"/>

	<!-- BOT TAGS -->
	<meta name="revisit-after" content="2 days"/>
	
		
	<link rel="apple-touch-icon" sizes="57x57" href="<?php  echo get_template_directory_uri() ?>/images/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php  echo get_template_directory_uri() ?>/images/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php  echo get_template_directory_uri() ?>/images/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php  echo get_template_directory_uri() ?>/images/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php  echo get_template_directory_uri() ?>/images/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php  echo get_template_directory_uri() ?>/images/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php  echo get_template_directory_uri() ?>/images/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php  echo get_template_directory_uri() ?>/images/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php  echo get_template_directory_uri() ?>/images/favicons/apple-touch-icon-180x180.png">	
			
		
		
	<link rel="icon" type="image/png" href="<?php  echo get_template_directory_uri() ?>/images/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php  echo get_template_directory_uri() ?>/images/favicons/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php  echo get_template_directory_uri() ?>/images/favicons/favicon-16x16.png" sizes="16x16">
	
	<link rel="manifest" href="<?php  echo get_template_directory_uri() ?>/images/favicons/manifest.json">
	<meta name="msapplication-TileColor" content="#00aba9">
	<meta name="msapplication-TileImage" content="<?php  echo get_template_directory_uri() ?>/images/favicons/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	
		
		
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	
	
	
	<?php endif; ?>
	<?php wp_head(); ?>
</head>


	<body id="lazyload-1">
	
		<div class="main-wrapper">
            <header id="header-position-1" class="header js-header">
				<?php get_template_part( 'template-parts/location-chooser' ); ?>


				<div class="header-inner">
					<div class="logo-container">
						<img src="<?php  echo get_template_directory_uri() ?>/images/logo.png" alt="myVacBot" class="header-logo"/>
						<h1><?php _e( 'myVacBot', 'myrobotcenter' ); ?></h1>
					</div>
					
					
					<?php
						wp_nav_menu( array( 
							'theme_location' => 'mrc-top-menu', 
							'container_class' => 'anchor-container',
							'container_id' => 'anchor-container',
							'container' => 'nav') ); 
								
						global $q_config;	

						//print_r($q_config);
						
						if (isset($q_config['language']))
							$language = $q_config['language'];
						else 
							$language = substr(get_bloginfo("language"),0, 2);;
								
					?>
					
			
					
					
					<div class="location-chooser">
						<div id="location-chooser-1" class="choose-location clickable">
							<span class="select-text">
								<?php _e( 'Choose country', 'myrobotcenter' ); ?>	
							</span>
							<span class="location-icon">
								<i class="flag-icon <?php echo $language; ?>"></i>
							</span>
						</div>
					</div>
					
					
					<div class="mobile-menu">
						<button id="mobile-menu-1" class="hamburger">
							<span class="hamburger-stripe"></span>
						</button>
					</div>
				</div>
			</header>
