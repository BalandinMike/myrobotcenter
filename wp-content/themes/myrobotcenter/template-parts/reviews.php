 
 <?php 
 
 
 ?>
 
 
 <div class="product-reviews">
            <div class="product-review-overlay"></div>

            <span class="is-hidden"
                  itemprop="aggregateRating"
                  itemscope
                  itemtype="http://schema.org/AggregateRating">
                <meta itemprop="ratingValue" content="4.3"/>
                <meta itemprop="reviewCount" content="3"/>
            </span>

            <div class="max-width">
                <div class="product-reviews-inner">
                    <h3 class="headline-third"><?php _e('Reviews', 'myrobotcenter'); ?></h3>

                    <div class="product-reviews-title-seperator">
                        <span class="seperator"></span>
                    </div>

                    <div class="product-reviews-text">                         
						<?php _e('What do customers think about this product?', 'myrobotcenter'); ?>	
					</div>

                    <div id="reviews-slider-<?php echo $post_no; ?>"
                         class="product-reviews-slider">
						 
							<?php foreach ($review_arr as $review) { ?>
						 
                            <div class="product-reviews-item" itemprop="review" itemscope itemtype="https://schema.org/Review">
                                <div class="grid grid--middle grid--rev">
                                    <div class="grid__item four-fifths special--one-whole palm--one-whole">
                                        <div class="bubble">
                                            <div class="top-container">
                                                <div class="quotation" itemprop="description">
                                                    &bdquo;<?php echo $review['text'] ?>&ldquo;
                                                </div>
                                            </div>

                                            <div class="bottom-container">
                                                <div class="user-info">
                                                    <meta itemprop="author"
                                                          content="<?php echo $review['author'] ?>"/>
                                                    &minus;&nbsp;<?php echo $review['author'] ?></div>
                                                <div class="rating"
                                                     itemprop="reviewRating"
                                                     itemscope
                                                     itemtype="https://schema.org/Rating">
                                                    
                                                    <meta itemprop="ratingValue" content="<?php echo $review['ratingValue'] ?>"/>

                                                        <i class="icon-star <?php echo rating_is_star_filled(1, $review['ratingValue'] ); ?>"></i>
                                                        <i class="icon-star <?php echo rating_is_star_filled(2, $review['ratingValue'] ); ?>"></i>
                                                        <i class="icon-star <?php echo rating_is_star_filled(3, $review['ratingValue'] ); ?>"></i>
                                                        <i class="icon-star <?php echo rating_is_star_filled(4, $review['ratingValue'] ); ?>"></i>
                                                        <i class="icon-star <?php echo rating_is_star_filled(5, $review['ratingValue'] ); ?>"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="grid__item one-fifth special--one-whole palm--one-whole">
                                        <div class="user-img-container">
                                            <div class="user-img"
                                                 style="background-image: url('<?php echo get_template_directory_uri() ?>/images/reviews/<?php echo $review['image'] ?>')">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
							
							<?php } ?>
							
						
							
                    </div>
                </div>
            </div>
</div>