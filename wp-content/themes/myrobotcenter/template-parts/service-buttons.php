
<?php $lang = substr(get_bloginfo("language"),0, 2); ?>

<section class="service-buttons-section">
                <div class="max-width">
                    <div class="grid grid--center">
                                <div class="grid__item one-quarter iphoneh--one-half palm--one-whole">
                                    <div class="service-buttons-item">
                                        <img alt="5-year guarantee"
                                             class="js-lazy service-buttons-item-img"
                                             data-original="<?php echo get_template_directory_uri() ?>/images/service-buttons/5-year-guarantee-<?php echo $lang; ?>.png"/>

                                        <div class="service-buttons-item-title">
                                             
											<?php _e( '5-year guarantee', 'myrobotcenter' ); ?>
										</div>

                                        <div class="service-buttons-item-text">
                                             
											<?php _e( 'Complete security over many years.', 'myrobotcenter' ); ?>
										</div>
                                    </div>
                                </div>
                                                                                                                <div class="grid__item one-quarter iphoneh--one-half palm--one-whole">
                                    <div class="service-buttons-item">
                                        <img alt="<?php _e( 'Test for 7 days', 'myrobotcenter' ); ?>"
                                             class="js-lazy service-buttons-item-img"
                                             data-original="<?php echo get_template_directory_uri() ?>/images/service-buttons/test-for-7-days-<?php echo $lang; ?>.png"/>

                                        <div class="service-buttons-item-title">
                                            
											<?php _e( 'Test for 7 days', 'myrobotcenter' ); ?>
										</div>

                                        <div class="service-buttons-item-text">
                                            
											<?php _e( 'Come and test the quality for yourself.', 'myrobotcenter' ); ?>
										</div>
                                    </div>
                                </div>
                                                                                                                <div class="grid__item one-quarter iphoneh--one-half palm--one-whole">
                                    <div class="service-buttons-item">
                                        <img alt="<?php _e( 'Free accessories', 'myrobotcenter' ); ?>"
                                             class="js-lazy service-buttons-item-img"
                                             data-original="<?php echo get_template_directory_uri() ?>/images/service-buttons/free-accessories-<?php echo $lang; ?>.png"/>

                                        <div class="service-buttons-item-title">
                                             
											<?php _e( 'Free accessories', 'myrobotcenter' ); ?>	
										</div>

                                        <div class="service-buttons-item-text">
                                            
											<?php _e( 'Free accessories for selected products.', 'myrobotcenter' ); ?>		
										</div>
                                    </div>
                                </div>
                                                                                                                <div class="grid__item one-quarter iphoneh--one-half palm--one-whole">
                                    <div class="service-buttons-item">
                                        <img alt="<?php _e( 'Free delivery', 'myrobotcenter' ); ?>"
                                             class="js-lazy service-buttons-item-img"
                                             data-original="<?php echo get_template_directory_uri() ?>/images/service-buttons/free-delivery-<?php echo $lang; ?>.png"/>

                                        <div class="service-buttons-item-title">
                                             
											<?php _e( 'Free delivery', 'myrobotcenter' ); ?>
										</div>

                                        <div class="service-buttons-item-text">                                             
											<?php _e( 'Delivery is made free of charge to the specified address for orders of over 100 GBP.', 'myrobotcenter' ); ?>
										</div>
                                    </div>
                                </div>
                               </div>
                </div>
            </section>