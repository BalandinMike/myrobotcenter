<div id="cookie-1" class="cookie-overlay">
    <div class="cookie-content clearfix">
        <div class="grid grid--full grid--middle">
            <div class="grid__item ten-twelfths ipadv--two-thirds iphoneh--two-thirds palm--one-whole">
                <p class="cookie-message"><?php _e( 'Cookie Message', 'myrobotcenter' ); ?> <a href="https://<?php _e( 'cookie_url', 'myrobotcenter' ); ?>"><?php _e( 'See details', 'myrobotcenter' ); ?></a></p>
            </div>
            <div class="grid__item two-twelfths ipadv--one-third iphoneh--one-third palm--one-whole">
                <span id="cookie-overlay-button" class="cookie-accept">
					<?php _e( 'Accept', 'myrobotcenter' ); ?>
				</span>
            </div>
        </div>
    </div>
</div>