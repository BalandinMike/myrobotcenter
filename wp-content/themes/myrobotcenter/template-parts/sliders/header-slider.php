<?php


?>

<section class="header-slider-section clearfix js-header-slider">
    <div id="header-slider-gradient" class="header-slider-gradient" ></div>

		<div id="header-slider-1" class="header-slider" >
       
				<div class="header-slider-item">
					<img src="<?php echo get_template_directory_uri() ?>/images/title-slider/title-image-01.png?v=1" alt="Die Roboter der Eigenmarke von myRobotcenter">
					<div class="header-slider-text-container">
						<div class="header-slider-title"> 
							<?php _e('Life is too short for bothersome housework.', 'myrobotcenter'); ?>                                        
						</div>

						<div class="header-slider-text">
							<?php _e('Intelligent household robots with innovative features.', 'myrobotcenter'); ?>                                        
						</div>
					</div>
				</div>
				
				<div class="header-slider-item">
					<img src="<?php echo get_template_directory_uri() ?>/images/title-slider/title-image-05.png?v=1" alt="Das Leben ist zu kurz für lästige Hausarbeiten.">
					<div class="header-slider-text-container">
						<div class="header-slider-title">
							<?php _e('myVacBot SN500 Robot Vacuum', 'myrobotcenter'); ?>                                        
						</div>

						<div class="header-slider-text">
							 <?php _e('The Robot Vacuum with intelligent Robart Navigation Technology.', 'myrobotcenter'); ?>                                           
						</div>
						<div id="anchor-link-4">
							<a href="#product-item-anchor-1" title="<?php _e('Find out more', 'myrobotcenter'); ?>" class="btn header-slider-button">
								<?php _e('Find out more', 'myrobotcenter'); ?>                                                
							</a>
						</div>

					</div>
				</div>
				
				<div class="header-slider-item">
					<img src="<?php echo get_template_directory_uri() ?>/images/title-slider/title-image-02.png?v=1?v=1" alt="myVacBot SN500 Staubsauger Roboter">
					<div class="header-slider-text-container">
						<div class="header-slider-title">
							<?php _e('myVacBot S200 Vacuum and Floor Mopping Robot', 'myrobotcenter'); ?>                                         
						</div>

						<div class="header-slider-text">
							<?php _e('The smart combo-talent for vacuuming and mopping work.', 'myrobotcenter'); ?>                                         
						</div>

						<div id="anchor-link-5">																													
							<a href="#product-item-anchor-2" title="<?php _e('Find out more', 'myrobotcenter'); ?>" class="btn header-slider-button">
								<?php _e('Find out more', 'myrobotcenter'); ?>                                                
							</a>
						</div>
					</div>
				</div>
				
				
				<div class="header-slider-item">
					<img src="<?php echo get_template_directory_uri() ?>/images/title-slider/title-image-03.png?v=1" alt="myVacBot S200 Saug und Wischroboter">
					<div class="header-slider-text-container">
					<div class="header-slider-title">
						<?php _e('myVacBot B100 Vacuum Robot', 'myrobotcenter'); ?>                                         
					</div>

					<div class="header-slider-text">
						<?php _e('The innovative household helper with new brush design.', 'myrobotcenter'); ?>                                         
					</div>

					<div id="anchor-link-6">	
						<a href="#product-item-anchor-3" title="<?php _e('Find out more', 'myrobotcenter'); ?>" class="btn header-slider-button">
							<?php _e('Find out more', 'myrobotcenter'); ?>                                                
						</a>
					</div>
																																</div>
				</div>
				
				
				<div class="header-slider-item">
					<img src="<?php echo get_template_directory_uri() ?>/images/title-slider/title-image-04.png?v=1" alt="Der myVacBot B100 Saugroboter">
					<div class="header-slider-text-container">
						<div class="header-slider-title">
							<?php _e('The robots of the Home Brand of myRobotcenter', 'myrobotcenter'); ?>                                       
						</div>

						<div class="header-slider-text">
							<?php _e('Quality at a convincing Price-Performance Ratio.', 'myrobotcenter'); ?>                                           
						</div>

						
					</div>
				</div>
					
				                                                                                                                                           
        
		</div>
</section>