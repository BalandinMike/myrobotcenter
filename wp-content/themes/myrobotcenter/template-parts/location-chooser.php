<div id="location-chooser-container" class="location-chooser-container">
        <div class="max-width">
            <div class="location-chooser-title">                
				<?php _e( 'Please select your country', 'myrobotcenter' ); ?>				
			
			
			</div>

            <div class="grid grid--middle text-alignment">
                
				<?php 
				
					if (function_exists('qtranxf_getSortedLanguages')) {
						
						global $q_config;
			
						foreach(qtranxf_getSortedLanguages() as $language) {
							
							$lang_url = qtranxf_convertURL('', $language, false, true);
							$lang_label = $q_config['language_name'][$language];
							
						?>	
						
							<div class="grid__item one-sixth iphoneh--one-half palm--one-whole">
								<a href="<?php echo $lang_url; ?>"
								   title="<?php echo $lang_label; ?>"
								   class="location-chooser-item">
									<span class="location-icon">
										<i class="flag-icon <?php echo $language; ?>"></i>
									</span>
									<span class="select-text">
										<?php echo $lang_label; ?>                       </span>
								</a>
							</div>
							
						<?php	
							
						}
						
						
						
					}
				
				
				?>
				
             
            </div>
		</div>
</div>		