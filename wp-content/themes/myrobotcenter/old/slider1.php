<?php


?>

<section class="header-slider-section clearfix js-header-slider">
    <div id="header-slider-gradient" class="header-slider-gradient" ></div>

    <div id="header-slider-1" class="header-slider" >
        <div aria-live="polite" class="slick-list draggable">
			<div class="slick-track" style="opacity: 1; width: 7560px; transform: translate3d(-4320px, 0px, 0px);" role="listbox">
		
				<div class="header-slider-item slick-slide slick-cloned" data-slick-index="-1" aria-hidden="true" style="width: 1080px;" tabindex="-1">
					<img src="<?php echo get_template_directory_uri() ?>/img/title-slider/title-image-04.png?v=1" alt="Die Roboter der Eigenmarke von myRobotcenter">
					<div class="header-slider-text-container">
						<div class="header-slider-title">
							Die Roboter der Eigenmarke von myRobotcenter                                        
						</div>

						<div class="header-slider-text">
							Qualität zu einem hervorragenden Preis-Leistungs-Verhältnis.                                        
						</div>
					</div>
				</div>
				
				<div class="header-slider-item slick-slide" data-slick-index="0" aria-hidden="true" style="width: 1080px;" tabindex="-1" role="option" aria-describedby="slick-slide00">
					<img src="<?php echo get_template_directory_uri() ?>/img/title-slider/title-image-01.png?v=1" alt="Das Leben ist zu kurz für lästige Hausarbeiten.">
					<div class="header-slider-text-container">
						<div class="header-slider-title">
							Das Leben ist zu kurz für lästige Hausarbeiten.                                        
						</div>

						<div class="header-slider-text">
							Intelligente Haushaltsroboter mit innovativen Funktionen.                                        
						</div>

					</div>
				</div>
				
				<div class="header-slider-item slick-slide" data-slick-index="1" aria-hidden="true" style="width: 1080px;" tabindex="-1" role="option" aria-describedby="slick-slide01">
					<img src="<?php echo get_template_directory_uri() ?>/img/title-slider/title-image-05.png?v=1?v=1" alt="myVacBot SN500 Staubsauger Roboter">
					<div class="header-slider-text-container">
						<div class="header-slider-title">
							myVacBot SN500 Staubsauger Roboter                                        
						</div>

						<div class="header-slider-text">
							Der Staubsauger Roboter mit intelligenter Robart Navigations-Technologie.                                        
						</div>

																																			
						<a id="anchor-link-4" href="#product-item-anchor-1" title="Mehr Infos" class="btn header-slider-button" tabindex="-1">
							Mehr Infos                                                
						</a>
					</div>
				</div>
				
				
				<div class="header-slider-item slick-slide" data-slick-index="2" aria-hidden="true" style="width: 1080px;" tabindex="-1" role="option" aria-describedby="slick-slide02">
					<img src="<?php echo get_template_directory_uri() ?>/img/title-slider/title-image-02.png?v=1" alt="myVacBot S200 Saug und Wischroboter">
					<div class="header-slider-text-container">
					<div class="header-slider-title">
						myVacBot S200 Saug und Wischroboter                                        
					</div>

					<div class="header-slider-text">
						Das smarte Kombitalent erledigt Wisch- und Saugarbeiten. Mit App-Steuerung.                                        
					</div>

					<a id="anchor-link-5" href="#product-item-anchor-2" title="Mehr Infos" class="btn header-slider-button" tabindex="-1">
						Mehr Infos                                                
					</a>
																																</div>
				</div>
				
				
				<div class="header-slider-item slick-slide slick-current slick-active" data-slick-index="3" aria-hidden="false" style="width: 1080px;" tabindex="-1" role="option" aria-describedby="slick-slide03">
					<img src="<?php echo get_template_directory_uri() ?>/img/title-slider/title-image-03.png?v=1" alt="Der myVacBot B100 Saugroboter">
					<div class="header-slider-text-container">
						<div class="header-slider-title">
							Der myVacBot B100 Saugroboter                                        
						</div>

						<div class="header-slider-text">
							Der innovative Haushaltshelfer mit neuem Bürstendesign.                                        
						</div>

						<a id="anchor-link-6" href="#product-item-anchor-3" title="Mehr Infos" class="btn header-slider-button" tabindex="0">
							Mehr Infos                                                
						</a>
					</div>
				</div>
					
				<div class="header-slider-item slick-slide" data-slick-index="4" aria-hidden="true" style="width: 1080px;" tabindex="-1" role="option" aria-describedby="slick-slide04">
					<img src="<?php echo get_template_directory_uri() ?>/img/title-slider/title-image-04.png?v=1" alt="Die Roboter der Eigenmarke von myRobotcenter">
					<div class="header-slider-text-container">
						<div class="header-slider-title">
							Die Roboter der Eigenmarke von myRobotcenter                                        
						</div>

						<div class="header-slider-text">
							Qualität zu einem hervorragenden Preis-Leistungs-Verhältnis.                                        
						</div>

					</div>
				</div>
				
				<div class="header-slider-item slick-slide slick-cloned" data-slick-index="5" aria-hidden="true" style="width: 1080px;" tabindex="-1">
					<img src="<?php echo get_template_directory_uri() ?>/img/title-slider/title-image-01.png?v=1" alt="Das Leben ist zu kurz für lästige Hausarbeiten.">
					<div class="header-slider-text-container">
						<div class="header-slider-title">
							Das Leben ist zu kurz für lästige Hausarbeiten.                                        
						</div>

						<div class="header-slider-text">
							Intelligente Haushaltsroboter mit innovativen Funktionen.                                        
						</div>

					</div>
				</div>
			</div>	
		</div>
                                                                                                                                                            
        <ul class="slick-dots" style="" role="tablist">
			<li class="" aria-hidden="true" role="presentation" aria-selected="true" aria-controls="navigation00" id="slick-slide00">
				<button type="button" data-role="none" role="button" tabindex="0">1</button>
			</li>
			<li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation01" id="slick-slide01" class="">
				<button type="button" data-role="none" role="button" tabindex="0">2</button>
			</li>			
			<li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation02" id="slick-slide02" class="">
				<button type="button" data-role="none" role="button" tabindex="0">3</button>
			</li>
			<li aria-hidden="false" role="presentation" aria-selected="false" aria-controls="navigation03" id="slick-slide03" class="slick-active">
				<button type="button" data-role="none" role="button" tabindex="0">4</button>
			</li>
			<li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation04" id="slick-slide04" class="">
				<button type="button" data-role="none" role="button" tabindex="0">5</button>
			</li>
		</ul>
	</div>
</section>